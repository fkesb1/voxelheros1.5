﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
using LitJson;
using System;

public class Rank : MonoBehaviour
{
    public void upRank(string teamname, int maxwave)
    {
        HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://nefus.kr/woh/voxel/setValue.php?teamname=" + teamname + "&f&maxwave=" + maxwave);
        try
        {
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                Debug.Log("success");
            }
            else if (myHttpWebResponse.StatusCode == HttpStatusCode.BadRequest)
            {
                Debug.Log("other name go");
            }
            else
            {
                Debug.Log("unknown error");
            }
            myHttpWebResponse.Close();
        }
        catch (Exception)
        {
            Debug.Log("success");
        }

    }
    public void getRank()
    {
        HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://nefus.kr/woh/voxel/getInfo.php");
        try
        {
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = myHttpWebResponse.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string data = readStream.ReadToEnd();
                JsonData getData = JsonMapper.ToObject(data);
                for (int i = 0; i < getData.Count; i++)
                {
                    Debug.Log("teamname:" + getData[i]["teamname"]);
                    Debug.Log("maxwave:" + getData[i]["maxwave"]);
                }
            }
            else
            {
                Debug.Log("unknown error");
            }
            myHttpWebResponse.Close();
        }
        catch (Exception)
        {
            Debug.Log("unknown error");
        }
    }

}