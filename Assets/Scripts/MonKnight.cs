﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MonKnight : NetworkManager_Custom {
    private CastleHp castlehp;

    private AudioSource audio;
    public AudioClip atkSound;

    private Animator anim;
    private float speed = 20.0f;
    private int damage = 10;

    public bool isDead = false;

	void Start ()
    {
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        castlehp = GameObject.Find("Castle_new_Ver").GetComponent<CastleHp>();
	}
    

    void Update()
    {
        if (transform.position.z > 103 && !isDead)
        {
            if (anim.GetBool("isAttack").Equals(true))
                anim.SetBool("isAttack", false);
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        else
        {
            if (anim.GetBool("isAttack").Equals(false))
                anim.SetBool("isAttack", true);
        }
    }
    
    void DamageCastle()
    {
        audio.PlayOneShot(atkSound);
        Packet p = new Packet((int)PacketType.OP_CastleDamage,false,damage.ToString(),userInfo.code);
        sendData(p);
    }
}
