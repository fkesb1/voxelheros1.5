﻿using UnityEngine;
using System.Collections;

public class OrbitalStrikeBullet : MonoBehaviour {

    public int damage;

    void OnTriggerEnter(Collider col)
    {
        var hit = col.gameObject;
        var hitEnemy = hit.GetComponent<EnemyHP>();

        if (hitEnemy != null)
        {
            hitEnemy.GetDamage();
        }
    }
}
