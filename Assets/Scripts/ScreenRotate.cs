﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScreenRotate : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    private Character player;

    public string playerid;

    // Use this for initialization
    void Start () {

        StartCoroutine("SetPlayer", 1f);
    }
    
    IEnumerator SetPlayer(float wt)
    {
        yield return new WaitForSeconds(wt);
        player = GameObject.Find("Player " + playerid).GetComponent<Character>();
    }

    public void OnPointerDown(PointerEventData data)
    {

    }

    public void OnPointerUp(PointerEventData data)
    {

    }

    public void OnDrag(PointerEventData data)
    {

    }
}
