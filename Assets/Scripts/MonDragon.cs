﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MonDragon : NetworkManager_Custom {

    public GameObject fireballPrefab;
    public GameObject firepos;

    private AudioSource audio;
    public AudioClip atkSound;

    private Animator anim;
    private float speed = 17.0f;

    public bool isDead = false;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (transform.position.z > 150 && !isDead)
        {
            if(anim.GetBool("isAttack").Equals(true))
                anim.SetBool("isAttack", false);
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        else
        {
            if (anim.GetBool("isAttack").Equals(false))
                anim.SetBool("isAttack", true);
        }
    }

    void ShootFire()
    {
        audio.PlayOneShot(atkSound);
        GameObject fire = (GameObject)Instantiate(fireballPrefab, firepos.transform.position, Quaternion.identity);
        fire.GetComponent<Rigidbody>().velocity = -fire.transform.forward * 30;
    }
}
