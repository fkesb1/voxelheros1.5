﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyMove : MonoBehaviour {
	private GameObject castle;
	private bool isAtk;
	public float dist;
	float speed = 25.0f;

	void Start () {
		castle = GameObject.Find ("Dead");
	}
	
	void Update () {
		dist = (castle.transform.position - transform.position).sqrMagnitude;
		if (dist >= 3000.0f) {
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards (transform.position, castle.transform.position, step);	
		}

	}
    
}
