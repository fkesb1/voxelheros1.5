﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class FireBtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public string playerid;
    public PlayerInfo teamInfo;

    private Character player;

    private bool isFire = false;

    private AudioSource aus;
    public AudioClip shootAudio;

	void Start ()
    {
        aus = GetComponent<AudioSource>();
        StartCoroutine("SetPlayer", 1.0f);
	}

	IEnumerator SetPlayer(float wt)
    {
        yield return new WaitForSeconds(wt);
        player = GameObject.Find(userInfo.playerid).GetComponent<Character>();
        //player = GameObject.Find("Player " + playerid).GetComponent<Character>();
        //userInfo.playerid = "Player " + playerid;
    }

    IEnumerator Fire(float wt)
    {
        if (!isFire) yield break;
        player.shoot(5);
        aus.PlayOneShot(shootAudio);
        yield return new WaitForSeconds(wt);
        StartCoroutine("Fire", 0.2f);
    }

    public void OnPointerDown(PointerEventData data)
    {
        isFire = true;
        StartCoroutine("Fire", 0.2f);
    }

    public void OnPointerUp(PointerEventData data)
    {
        isFire = false;
    }
}
