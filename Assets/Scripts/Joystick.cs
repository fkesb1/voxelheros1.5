﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Joystick : NetworkManager_Custom, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    private Character player;

    public Transform stick;

    private Vector3 axis;

    private float radius;
    private Vector3 defaultCenter;
    void Awake()
    {
        radius = GetComponent<RectTransform>().sizeDelta.y * 0.5f;
        defaultCenter = stick.position;
    }

    void Start()
    {
        defaultCenter = transform.position;
        StartCoroutine("SetPlayer", 1f);
    }

    IEnumerator SetPlayer(float wt)
    {
        yield return new WaitForSeconds(wt);
        player = GameObject.Find(userInfo.playerid).GetComponent<Character>();
    }
    void Movement(Vector3 touchPos)
    {
        axis = (touchPos - defaultCenter).normalized;
        float fDistance = Vector3.Distance(touchPos, defaultCenter);
        if (fDistance > radius)
            stick.transform.position = defaultCenter + axis * radius;
        else
            stick.transform.position = defaultCenter + axis * fDistance;
        locationSend();
    }
    private void locationSend()
    {
        PlayerMove m = new PlayerMove(userInfo.playerid, player.transform.position.x, player.transform.position.y, player.transform.position.z, player.transform.rotation.x, player.transform.rotation.y, player.transform.rotation.z);
        Packet p = new Packet((int)PacketType.OP_PlayerMove, false, JsonUtility.ToJson(m), userInfo.code);
        sendData(p);
    }
    public void OnPointerDown(PointerEventData data)
    {
        Movement(data.position);
        player.JoystickMoveCtrl(axis, true);
    }

    public void OnPointerUp(PointerEventData data)
    {
        axis = Vector3.zero;
        stick.transform.position = defaultCenter;
        player.JoystickMoveCtrl(axis, false);
    }

    public void OnDrag(PointerEventData data)
    {
        Movement(data.position);
        player.JoystickMoveCtrl(axis, true);
    }
}
