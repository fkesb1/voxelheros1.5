﻿using UnityEngine;
using System.Collections;

public class RoomButton : MonoBehaviour {

    public Canvas roomCanvas;

    private bool isOpen = false;

	public void OpenRoomCanvas()
    {
        isOpen = !isOpen;
        roomCanvas.enabled = isOpen;
    }
}
