﻿using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using System.Text;
using UnityEngine.UI;
using System;

public class userData : NetworkManager_Custom{

    public Text expText;
    public Text nameText;
    public Text maxwaveText;
    public Text playcountText;

    public Text errorText;

    public Canvas loginCanvas;
    public InputField usernameInput;
    public InputField passwordInput;
    private string username;
    private string password;
    private float version=1.0f;

    private void setTexts()
    {
        nameText.text = username;
    }
    private string path;
    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
        if (Application.platform == RuntimePlatform.Android)
        {
            path = Application.persistentDataPath;
        }
        else
        {
            path = "./";
        }
        loginCanvas.enabled = true;
        readFile();
        SetLoginInfo();
    }
    private void readFile()
    {
        try
        {
            StreamReader r = new StreamReader(path);
            string data = r.ReadLine();
            LoginData l = JsonUtility.FromJson<LoginData>(data);
            usernameInput.text = l.username;
            passwordInput.text = l.password;
        }
        catch (Exception)
        {

        }
    }
    public void SetLoginInfo()
    {
        if (usernameInput.text.Length == 0 || passwordInput.text.Length == 0)
        {
            errorText.enabled = true;
        }
        else
        {
            username = usernameInput.text;
            password = passwordInput.text;
            if (login() == 0)
            {
                loginCanvas.enabled = false;
            }
            else
            {
                errorText.enabled = true;
            }
        }
    }
    private int login()
    {
        byte[] buffer = new byte[1024];
        byte[] buffer2 = new byte[1024];
        username = usernameInput.text;
        password = passwordInput.text;
        Login l = new Login(username, password, version);
        Packet p = new Packet((int)PacketType.OP_login, false,JsonUtility.ToJson(l), "");
        buffer = Encoding.UTF8.GetBytes(JsonUtility.ToJson(p));
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            Packet p2 = JsonUtility.FromJson<Packet>(Encoding.UTF8.GetString(buffer2));
            LoginReply r = JsonUtility.FromJson<LoginReply>(p2.body);
            if (p2.packetType == (int)PacketType.OP_loginReply && r.code==0)
            {
                userInfo.playerid = username;
                userInfo.privilege = r.privilege;
                return 0;
            }
            Debug.Log("wrond id or pw");
            return 1;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return -1;
        }
    }
    public void register()
    {
        byte[] buffer = new byte[1024];
        byte[] buffer2 = new byte[1024];
        Login l = new Login(username, password, version);
        Packet p = new Packet((int)PacketType.OP_CreateAccount, false, JsonUtility.ToJson(l), "");
        buffer = Encoding.UTF8.GetBytes(JsonUtility.ToJson(p));
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            Packet p2 = JsonUtility.FromJson<Packet>(Encoding.UTF8.GetString(buffer2));
            if (p2.packetType == (int)PacketType.OP_CreateAccountReply && p2.body=="0")
            {
                userInfo.playerid = username;
                errorText.text = "가입완료. 로그인해주세요";
                return;
            }
            errorText.text = "가입실패 다시시도해주세요";
            return;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return;
        }
    }
}