﻿using UnityEngine;
using System.Collections;

public class CanvasLookat : MonoBehaviour {
    public Transform lookPoint;

    void Awake()
    {
        lookPoint = GameObject.Find("SpawnPoints").transform;
        transform.LookAt(lookPoint);
    }
}
