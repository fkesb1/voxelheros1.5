﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDamage : MonoBehaviour {

	void OnTriggerEnter(Collider col)
    {
        Debug.Log("trigger Enter");
        var hit = col.gameObject;
        var hitEnemy = hit.GetComponent<EnemyHP>();

        if (hitEnemy != null)
        {
            Debug.Log("hit");
            hitEnemy.GetDamage();
            Destroy(gameObject);
        }
    }
}
