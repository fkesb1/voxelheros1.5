﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Character : GameManager
{


    /* **************** 게임 오브젝트 **************** */

    // 플레이어 애니메이터
    private Animator anim;
    // 플레이어 카메라
    public Camera cam;
    // 플레이어 머리
    public GameObject head;

    // 총알
    public GameObject bulletPrefab;
    // 총알 발사위치
    public GameObject Firepos;
    public GameObject shootEffect;

    private Slider sensivitySlider;

    /* *********************************************** */


    
    /* ****************** 스크립트 ******************* */
    
    private Joystick joy;
    private FireBtn fireBtn;
    private PlayerInfo teamInfo;

    /* *********************************************** */



    /* ******************* 캐릭터 ******************** */

    // 캐릭터 이동속도
    private float mSpeed = 12f;

    // x, y축 회전 감도
    private float sensivityX;
    private float sensivityY;

    // y축 회전 최대, 최소치
    private int maxY = 70;
    private int minY = -70;

    // y축 회전 저장 변수
    private float rotateY = 0;

    // Axis 키 입력 변수
    private float hor;
    private float ver;

    // 조이스틱 포지션 벡터 함수
    private Vector3 JoystickPosition;

    /* *********************************************** */



    /* *************** 터치 관련 변수 **************** */

    // 화면 이동 터치가 먼저인지 확인하는 변수
    public bool isTouchScreenFirst;

    // 위 변수 임시 저장
    private bool tmpTouchFirst;

    /* *********************************************** */


        

    private string id;

    public static GameObject deathcam;

    public void changeSensivity()
    {
        sensivityX = sensivitySlider.value;
        sensivityY = sensivitySlider.value;
        Debug.Log(sensivityX);
        Debug.Log(sensivityY);
    }

    public void Start()
    {
        if(Screen.width.Equals(2560) && Screen.height.Equals(1440))
        {
            sensivityX = 0.5f;
            sensivityY = 0.5f;
        }
        else if(Screen.width.Equals(1920) && Screen.height.Equals(1080))
        {
            sensivityX = 1f;
            sensivityY = 1f;
        }
        else if(Screen.width.Equals(1280) && Screen.height.Equals(720))
        {
            sensivityX = 1.5f;
            sensivityY = 1.5f;
        }
        else
        {
            sensivityX = 0.5f;
            sensivityY = 0.5f;
        }

        deathcam = GameObject.Find("Camera");
        deathcam.SetActive(false);
        cam.enabled = true;
        GetComponent<AudioListener>().enabled = true;
        sensivitySlider = GameObject.Find("Slider_Sensivity").GetComponent<Slider>();
        sensivitySlider.onValueChanged.AddListener(delegate { changeSensivity(); });;
        //teamInfo = GameObject.Find("TeamInfo").GetComponent<PlayerInfo>();
        fireBtn.playerid = id;
        //InvokeRepeating("locationSend", 0.05f, 0.05f);
    }

    void Awake()
    {
        joy = GameObject.Find("Joypad").GetComponent<Joystick>();
        fireBtn = GameObject.Find("Btn_Attack").GetComponent<FireBtn>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (Input.touchCount == 1 || tmpTouchFirst)
            {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    isTouchScreenFirst = true;
                    tmpTouchFirst = isTouchScreenFirst;
                }
            }

            if (isTouchScreenFirst)
            {
                touch = Input.GetTouch(0);
            }
            else
            {
                touch = Input.GetTouch(1);
            }


            if (touch.phase == TouchPhase.Moved)
            {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    transform.Rotate(0, touch.deltaPosition.x * sensivityX, 0);
                    rotateY += touch.deltaPosition.y * sensivityY;
                    rotateY = Mathf.Clamp(rotateY, minY, maxY);
                    cam.transform.localEulerAngles = new Vector3(-rotateY, cam.transform.localEulerAngles.y, 0);
                    head.transform.localEulerAngles = new Vector3(-rotateY, head.transform.localEulerAngles.y, 0);
                }
            }

            if (touch.phase == TouchPhase.Ended)
            {
                isTouchScreenFirst = false;
            }

        }

        if (Input.touchCount == 0)
            tmpTouchFirst = false;
    }   
    public void JoystickMoveCtrl(Vector3 position, bool endTouch)
    {

        if (!endTouch)
            anim.SetBool("isRun", true);
        else
            anim.SetBool("isRun", false);

        StopCoroutine("JoystickActive");
        StartCoroutine("JoystickActive", position);

    }

    IEnumerator JoystickActive(Vector3 position)
    {
        while (true)
        {
            transform.Translate(new Vector3(position.x, 0, position.y) * mSpeed * Time.deltaTime);
            yield return null;
        }
    }
    
    public void shoot(int damage)
    {
        //anim.SetBool("isShoot", true);
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, Firepos.transform.position, cam.transform.rotation);
        GameObject shootEffectq = (GameObject)Instantiate(shootEffect, Firepos.transform.position, cam.transform.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 500;
        bullet.GetComponent<Bullet>().damage = damage;
        Destroy(shootEffectq, 0.5f);
        Destroy(bullet, 3.0f);
    }
}
