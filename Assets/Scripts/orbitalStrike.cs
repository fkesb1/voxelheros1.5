﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class orbitalStrike : MonoBehaviour
{

    public GameObject bulletPrefab;
    public GameObject effect;
    public GameObject air;

    public AudioClip boomaudio;
    private AudioSource audios;

    void Start () {
        audios = GetComponent<AudioSource>();
        InvokeRepeating("launch1", 0.1f, 0.2f);
        StartCoroutine("BoomSound", 1.0f);
    }
    void launch1()
    {
        for (int i = 0; i < 5; i++)
        {
            System.Random r = new System.Random();
            int x = r.Next(4,100);
            int y = r.Next(4, 40);
            GameObject shooteffect = (GameObject)Instantiate(effect, air.transform.position + new Vector3(x, y, 0), Quaternion.Euler(new Vector3(180, 0, 0)));
            Destroy(shooteffect, 2.0f);
        }
    }

    IEnumerator BoomSound(float wt)
    {
        yield return new WaitForSeconds(wt);

        audios.PlayOneShot(boomaudio);

        StartCoroutine("BoomSound", 0.3f);
    }
}
