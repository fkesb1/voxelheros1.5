﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class EnemyHP : NetworkManager_Custom {

    private Animator anim;

    public Slider hpSlider;

    private int wave;
    private int strengthenEnemy;

    public int hp;

    private int whoami; // Knight = 1, Dragon = 2, Golem = 3

    private AudioSource audio;
    public AudioClip hitSound;
    private Slider volumeSlider;
    private Toggle volumeToggle;

    private PlayerInfo teamInfo;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        volumeSlider = GameObject.Find("Slider_EffectSound").GetComponent<Slider>();
        volumeSlider.onValueChanged.AddListener(delegate { VolumeControl(); });
        volumeToggle = GameObject.Find("Toggle_EffectSound").GetComponent<Toggle>();
        volumeToggle.onValueChanged.AddListener(delegate { VolumeOnOff(); });
        hp = 100;
        hpSlider.value = hp;
        teamInfo = GameObject.Find("TeamInfo").GetComponent<PlayerInfo>();
    }

    public void VolumeControl()
    {
        audio.volume = volumeSlider.value;
    }

    public void VolumeOnOff()
    {
        audio.enabled = volumeToggle.isOn;
    }

    public void GetDamage()
    {
        EnemyShoot e = new EnemyShoot(int.Parse(this.name));
        Packet p = new Packet((int)PacketType.OP_EnemyHit,false,JsonUtility.ToJson(e),userInfo.code);
        sendData(p);
    }
    public void GetDamage(int hp)
    {
        audio.PlayOneShot(hitSound);

        this.hp = hp;
        hpSlider.value = this.hp;
        if (this.hp <= 0)
        {
            GetComponent<BoxCollider>().enabled = false;
            anim.SetBool("isDeath", true);
            teamInfo.GetComponent<PlayerInfo>().KillTextUpdate();
            if (whoami.Equals(1))
            {
                GetComponent<MonKnight>().isDead = true;
            }
            else if (whoami.Equals(2))
            {
                GetComponent<MonDragon>().isDead = true;
            }
            else if (whoami.Equals(3))
            {
                GetComponent<MonGolem>().isDead = true;
            }
            Destroy(this.gameObject,2.0f);
        }
        else
        {
            transform.Translate(new Vector3(0, 0, -5f));
        }
    }
}
