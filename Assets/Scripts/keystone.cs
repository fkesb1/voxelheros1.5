﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class keystone : NetworkManager_Custom
{
    public GameObject location;
    public GameObject airplaneObj;
    public GameObject missile;
    public GameObject IONcannonLA;


    private float lifetime = 3.5f;
    private int spd = 85;
    
    private int timer = 0;
    private bool shootReady = false;

    private Renderer render;

    void Start()
    {
        render = GetComponent<Renderer>();
        render.material.color = Color.red;
    }

    void OnMouseDown()
    {
        Packet p = new Packet((int)PacketType.OP_OrbitalStrikeRequest,false,userInfo.code,"");
        sendData(p);
    }
    public void launch()
    {
        StartCoroutine("Shoot",5);
    }
    IEnumerator Shoot(float wt)
    {
        shootReady = false;
        //RpcChangeColor(Color.red);
        StartCoroutine("CoolTime");
        GameObject airplane = (GameObject)Instantiate(airplaneObj, location.transform.position, Quaternion.identity);
        airplane.GetComponent<Rigidbody>().velocity = airplane.transform.forward * 120;
        NetworkServer.Spawn(airplane);
        Destroy(airplane, lifetime);

        yield return new WaitForSeconds(wt);

        GameObject ION1 = (GameObject)Instantiate(missile, IONcannonLA.transform.position, Quaternion.identity);
        ION1.GetComponent<Rigidbody>().velocity = ION1.transform.forward * spd;
        //ION1.GetComponent<OrbitalStrikeBullet>().damage = 1000;
        Destroy(ION1, lifetime);
    }
}
