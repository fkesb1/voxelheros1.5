﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Net.NetworkInformation;

public class NetworkManager_Custom : MonoBehaviour
{
    private Canvas loadingCanvas;
    private Text joinErrorText;
    public Text addrText;
    public Text serverNameText;
    public Text serverTypeText;
    public Text connectTypeText;
    public Text latencyText;
    private bool isError = false;
    protected static Socket s;
    protected static IPEndPoint ipepW;
    protected static EndPoint remoteEpW;
    //private string addr = "45.76.207.225";
    private string addr = "115.68.184.224";
    private string ip;

    private SocketAsyncEventArgs gamePacketSend;
    private Queue<Packet> que;
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    private void Start()
    {
        s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        s.SendTimeout = 4000;
        s.ReceiveTimeout = 4000;
        ipepW = new IPEndPoint(IPAddress.Parse(addr), 51000);
        remoteEpW = (EndPoint)ipepW;
        addrText.text = addr;
        getStatus();
    }
    public void getStatus()
    {
        Packet p = new Packet((int)PacketType.OP_ServerStatus, false, "", "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            if (p2.packetType != (int)PacketType.OP_ServerStatusReply)
            {
                connectTypeText.text = "Error";
            }
            ServerStatus st = JsonUtility.FromJson<ServerStatus>(p2.body);
            serverNameText.text = st.serverName;
            serverTypeText.text = st.mode;
        }
        catch (Exception)
        {
            connectTypeText.text = "FAIL";
        }
    }
    public void sendData(Packet p)
    {
        byte[] buffer = new byte[1024];
        buffer = Encoding.UTF8.GetBytes(JsonUtility.ToJson(p));
        gamePacketSend = new SocketAsyncEventArgs();
        gamePacketSend.RemoteEndPoint = ipepW;
        gamePacketSend.SetBuffer(buffer, 0, buffer.Length);
        s.SendToAsync(gamePacketSend);
    }
    IEnumerator SetupMenuSceneButtons()
    {
        yield return new WaitForSeconds(0.3f);
        loadingCanvas = GameObject.Find("Canvas_Loading").GetComponent<Canvas>();
        joinErrorText = GameObject.Find("Text_JoinError").GetComponent<Text>();
    }
    void login()
    {
        Packet p = new Packet((int)PacketType.OP_login, false, userInfo.playerid, "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            if (p2.packetType != (int)PacketType.OP_loginReply)
            {
                throw new Exception();
            }
            Debug.Log("login success"+data2);
        }
        catch (Exception)
        {
            Debug.Log("Login fail");
        }
    }
    public void JoinGame()
    {
        //login();
        //Debug.Log(userInfo.playerid);
        Packet p = new Packet((int)PacketType.OP_FindRoom, false, userInfo.playerid, "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            Debug.Log("join log " + data2);
            if (p2.packetType == (int)PacketType.OP_CreateRoomReply)
            {
                userInfo.code = p2.body;
                userInfo.host = true;
                SceneManager.LoadScene("CastleDef");
            }
            else if(p2.packetType == (int)PacketType.OP_FindRoomReply)
            {
                userInfo.code = p2.body;
                userInfo.host = false;
                SceneManager.LoadScene("CastleDef");
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception)
        {
            Debug.Log("fail to find(make) room");
        }
    }
    public void killRoom()
    {
        Packet p = new Packet((int)PacketType.OP_KillRoom, false, Network.player.ipAddress, "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            if (p2.packetType != (int)PacketType.OP_KillRoomReply)
            {
                throw new Exception();
            }
        }
        catch (Exception)
        {
            killRoom();
        }
    }
}
