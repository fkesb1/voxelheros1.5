﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetMyIp : MonoBehaviour {
    
	void Start () {
        Text ipText = GetComponent<Text>();
        ipText.text = "Room IP : " + Network.player.ipAddress;
	}
}
