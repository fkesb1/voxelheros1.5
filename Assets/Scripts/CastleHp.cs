﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using UnityEngine.SceneManagement;

public class CastleHp : MonoBehaviour {
    private Slider castleHpSlider;
    private Text textHp;
    
    public int maxHp;
    public int curHp;

    private StringBuilder sb = new StringBuilder();

    private bool isSave = false;

    private userData data;

    private GameObject can;

    public GameObject boomEffect;

    public Canvas defaultCanvas;
    public Canvas shopCanvas;
    public Canvas optionCanvas;

    private AudioSource audio;

    public Text killresultText;
    public Text maxkillresultText;
    public Text waveresultText;
    public Text maxwaveresultText;
    public Text scoreresultText;
    public Text curscoreresultText;
    public PlayerInfo teamInfo;

    public EnemySpawner enemySpawner;

    public AudioSource backAudio;
    public AudioClip deathSound;

    void Start ()
    {
        audio = GetComponent<AudioSource>();
        castleHpSlider = GameObject.Find("CastleHp").GetComponent<Slider>();
        textHp = GameObject.Find("Text_Hp").GetComponent<Text>();

        textHp.text = SetHpText();
        data = GameObject.Find("UserData").GetComponent<userData>();
        can = GameObject.Find("Canvas_GameOver");
    }

    public string SetHpText()
    {
        castleHpSlider.maxValue = this.maxHp;
        castleHpSlider.value = this.curHp;

        sb.Length = 0;

        sb.Append(curHp);
        sb.Append(" / ");
        sb.Append(maxHp);
        textHp.text = sb.ToString();
        return sb.ToString();
    }

    public void GameOver()
    {
        StartCoroutine("Death", 5.0f);
    }
    IEnumerator Death(float wt)
    {
        if (isSave.Equals(false))
        {
            isSave = true;

            enemySpawner.StopAllCoroutines();

            Debug.Log(teamInfo.kill);
            killresultText.text = teamInfo.kill.ToString();
            maxkillresultText.text = "최고 처치 : " ;
            waveresultText.text = enemySpawner.wave.ToString();
            //maxwaveresultText.text = "최고 웨이브 : " + data.getMaxWave().ToString();
            scoreresultText.text = (int.Parse(killresultText.text)*10 + int.Parse(waveresultText.text)*100).ToString();
            //curscoreresultText.text = "총 점수 : " + data.getExp().ToString();

            backAudio.Stop();
            defaultCanvas.enabled = false;
            shopCanvas.enabled = false;
            optionCanvas.enabled = false;
            backAudio.PlayOneShot(deathSound);
            Character.deathcam.SetActive(true);

            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < players.Length; i++)
            {
                Destroy(players[i].gameObject);
            }
            GameObject[] monsters = GameObject.FindGameObjectsWithTag("Monster");
            for (int i = 0; i < monsters.Length; i++)
            {
                Destroy(monsters[i].gameObject);
            }

            audio.Play();
            Instantiate(boomEffect, new Vector3(102, 44, 45), Quaternion.identity);

            yield return new WaitForSeconds(wt);

            audio.Stop();
            can.GetComponent<Canvas>().enabled = true;
            can.GetComponent<Animator>().SetBool("isGameOver", true);

            yield return new WaitForSeconds(15.0f);
            Destroy(data.gameObject);
            SceneManager.LoadScene("MainMenu");
        }
    }
}
