﻿using UnityEngine;
using LitJson;
using UnityEngine.UI;
using System.Text;
using System.Collections;

public class EnemySpawner : NetworkManager_Custom {
	public GameObject KnightPrefab;
    public GameObject DragonPrefab;
    public GameObject GolemPrefab;

    private int time = 0;
	private bool isWave = true;

    public int wave = 1;
    public Text textWave;
    public Text textTimer;
    private StringBuilder sb = new StringBuilder();
    public AudioClip waveStartaudio;
	void Start()
    {

    }
    public void spawnEnemy(string data)
    {
        Debug.Log(data);
        JsonData j = JsonMapper.ToObject(data);
        for(int i = 0; i < j.Count; i++)
        {
            GameObject tmp=GameObject.Find(j[i]["id"].ToString());
            if (tmp != null) continue;
            if ((int)j[i]["type"] == 1)
            {
                Vector3 spawnPoint = new Vector3((int)j[i]["x"], 3.6f, 400f);
                GameObject e = Instantiate(KnightPrefab, spawnPoint, Quaternion.Euler(0, 180, 0));
                e.name = j[i]["id"].ToString();
            }
            //else if ((int)j[i]["type"] == 2)
            //{
            //    Vector3 spawnPoint = new Vector3((int)j[i]["x"], 3.6f, 400f);
            //    GameObject e = Instantiate(DragonPrefab, spawnPoint, Quaternion.Euler(0, 180, 0));
            //    e.name = j[i]["id"].ToString();
            //}
            else if ((int)j[i]["type"] == 3)
            {
                Vector3 spawnPoint = new Vector3((int)j[i]["x"], 3.6f, 400f);
                GameObject e = Instantiate(GolemPrefab, spawnPoint, Quaternion.Euler(0, 180, 0));
                e.name = j[i]["id"].ToString();
            }
        }
        if (isWave) StartCoroutine("WaveTimer", 0f);
    }
    IEnumerator WaveTimer(float wt)
    {
        isWave = false;
        yield return new WaitForSeconds(wt);
        time++;
        textTimer.text=time.ToString();
        if (time.Equals(2) && this.wave!=userInfo.currentWave)
        {
            Packet p = new Packet((int)PacketType.OP_GameStatusRequest, false, userInfo.code, "");
            sendData(p);
        }
        if (time.Equals(25))
        {
            Packet p = new Packet((int)PacketType.OP_WaveEnd, false,wave.ToString(), userInfo.code);
            sendData(p);
            time = 0;
            textTimer.text=time.ToString();
        }
        StartCoroutine("WaveTimer", 1.0f);
    }
    public void setWave()
    {
        sb.Length = 0;
        sb.Append("Wave ");
        sb.Append(wave);
        textWave.text = sb.ToString();
    }
}
