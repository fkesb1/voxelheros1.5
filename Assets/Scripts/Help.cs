﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Help : MonoBehaviour {

    public Image helpImage;

    public Sprite[] help;
    

    private int i = 0;
    public void NextHelp()
    {
        i++;
        if (i.Equals(help.Length))
        {
            GetComponent<Canvas>().enabled = false;
            HelpClose();
        }
        helpImage.sprite = help[i];
    }

    public void HelpClose()
    {
        i = 0;
        helpImage.sprite = help[0];
    }
}
