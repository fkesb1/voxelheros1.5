﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerInfo : MonoBehaviour {

    public int damage = 20;
    public int kill = 0;
    public int money = 0;

    private Text killText;
    private Text moneyText;
    private Text curPower;
    private Text noMoneyText;

    private CastleHp castleHp;

    private bool isNoMoneyText = false;
    
    public AudioClip purcSuccess;
    public AudioClip purcFail;

    private AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        killText = GameObject.Find("Text_KillCount").GetComponent<Text>();
        moneyText = GameObject.Find("Text_Money").GetComponent<Text>();
        killText.text = kill.ToString();
        moneyText.text = money.ToString();

        curPower = GameObject.Find("Text_CurrPower").GetComponent<Text>();
        noMoneyText = GameObject.Find("Text_NoMoney").GetComponent<Text>();
        castleHp = GameObject.Find("Castle_new_Ver").GetComponent<CastleHp>();
    }
    
    public void KillTextUpdate(int kill)
    {
        killText.text = kill.ToString();
    }
    public void KillTextUpdate()
    {
        this.kill++;
        killText.text = kill.ToString();
    }
    public void MoneyTextUpdate(int money)
    {
        moneyText.text = money.ToString();
    }

    public void PowerTextUpdate()
    {
        curPower.text = damage.ToString();
    }

    public void UpgradePower()
    {
        if (money >= 1000)
        {
            audio.PlayOneShot(purcSuccess);
            GameObject.Find("GameManager").GetComponent<GameManager>().upgradeDamage();
        }
        else
        {
            audio.PlayOneShot(purcFail);
            if (isNoMoneyText.Equals(false))
                StartCoroutine("AppearNoMoneyText", 1.0f);
        }
    }

    public void UpgradeCastleHpTmp()
    {
        if (money >= 300)
        {
            audio.PlayOneShot(purcSuccess);
            GameObject.Find("GameManager").GetComponent<GameManager>().castleHpUpgrade();
        }
        else
        {
            audio.PlayOneShot(purcFail);
            if (isNoMoneyText.Equals(false))
                StartCoroutine("AppearNoMoneyText", 1.0f);
        }
    }

    public void RepairCastleHpTmp()
    {
        if (money >= 3000)
        {
            audio.PlayOneShot(purcSuccess);
            GameObject.Find("GameManager").GetComponent<GameManager>().repairHp();
        }
        else
        {
            audio.PlayOneShot(purcFail);
            if (isNoMoneyText.Equals(false))
                StartCoroutine("AppearNoMoneyText", 1.0f);
        }
    }

    IEnumerator AppearNoMoneyText(float wt)
    {
        isNoMoneyText = true;
        noMoneyText.enabled = true;
        yield return new WaitForSeconds(wt);
        noMoneyText.enabled = false;
        isNoMoneyText = false;
    }
}
