﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DragonFire : NetworkManager_Custom {
    private int damage = 5;

    void OnTriggerEnter(Collider col)
    {
        if(col.transform.CompareTag("Castle"))
        {
            Packet p = new Packet((int)PacketType.OP_CastleDamage, false, damage.ToString(), userInfo.code);
            sendData(p);
        }
    }
}
