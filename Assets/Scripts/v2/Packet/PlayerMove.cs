﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove {
    public string username;
    public float x;
    public float y;
    public float z;
    public float rx;
    public float ry;
    public float rz;
    public PlayerMove(string username,float x,float y,float z,float rx,float ry,float rz)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.username = username;
    }
}
