﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Missile : NetworkBehaviour {

    private float speed = 300f;
    public GameObject explosionEffect;
    private bool isExplode = false;

    void Start()
    {
        StartCoroutine("DestroyBullet", 5f);
    }

    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        if(transform.position.y < 0 && !isExplode)
        {
            isExplode = true;
            GameObject explosion = (GameObject)Instantiate(explosionEffect, transform.position, Quaternion.Euler(-90, 0, 0));
            NetworkServer.Spawn(explosion);
            Destroy(explosion.gameObject, 1.5f);
            Destroy(this.gameObject, 1);
        }
    }
    IEnumerator DestroyBullet(float wt)
    {
        yield return new WaitForSeconds(wt);

        Destroy(this.gameObject);
    }

    /*
    void OnTriggerEnter(Collider col)
    {
        if(col.transform.name.Equals("MissileExplode"))
        {
            Debug.Log("explode");
            GameObject explosion = (GameObject)Instantiate(explosionEffect, transform.position, Quaternion.Euler(0, 0, 0));
            Destroy(explosion.gameObject, 2f);
            Destroy(this.gameObject);
        }
    }
    */
}
