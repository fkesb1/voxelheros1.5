﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Earthquake : MonoBehaviour
{
    public Transform camTransform;

    public float shakeDuration = 10f;
    public float shakeAmount = 0.8f;
    public float decreaseFactor = 1.0f;
    Vector3 originalPos;
    private void Start()
    {
        camTransform = GameObject.Find(userInfo.playerid).transform.Find("PlayerCamera");
        originalPos = camTransform.localPosition;
    }
    void Update()
    {
        if (shakeDuration > 0)
        {
            camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            camTransform.localPosition = originalPos;
        }
    }
}
