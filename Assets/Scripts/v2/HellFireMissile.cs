﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HellFireMissile : NetworkBehaviour {
    public GameObject missile;
    public GameObject effect;
    public AudioClip launchSound;
    private AudioSource aus;
    // 위아래 기울기 17 ~ 50
    private int x;
    // 양옆 기울기 -30 ~ 30
    private int y;

    private int cnt = 0;
    private void Start()
    {
        aus = GetComponent<AudioSource>();
    }
    public void Fire()
    {
        Invoke("a", 0.9f);
    }
    public void a()
    {
        x = Random.Range(17, 50);
        y = Random.Range(-20, 20);
        GameObject missileobj = (GameObject)Instantiate(missile, transform.position, Quaternion.Euler(x, y, 0));
        NetworkServer.Spawn(missileobj);
        aus.PlayOneShot(launchSound);
        y = y - 6;
        cnt++;
        if (cnt % 20 == 0)
        {
            y = 30;
            cnt = 0;
        }
        else
        {
            Invoke("a", 0.3f);
        }
    }
}
