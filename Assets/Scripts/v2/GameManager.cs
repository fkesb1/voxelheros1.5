﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class GameManager : NetworkManager_Custom {
    public GameObject player;
    public GameObject EnemySpawner;
    public GameObject nature;
    private Queue<Packet> que;
    private byte[] buffer;
    private SocketAsyncEventArgs recv;
    private SocketAsyncEventArgs gamePacketSend;
    public GameObject missile;
    public GameObject start_btn;
    //-----------game object
    public GameObject castle;

    private void Start()
    {
        userInfo.currentWave = 0;
        buffer = new byte[1024];
        que = new Queue<Packet>();
        recv = new SocketAsyncEventArgs();
        recv.RemoteEndPoint = ipepW;
        recv.SetBuffer(buffer, 0, buffer.Length);
        recv.Completed += new EventHandler<SocketAsyncEventArgs>(recv_callback);

        Packet p = new Packet((int)PacketType.OP_GameStatusRequest, false, userInfo.code, "");
        sendData(p);
        s.ReceiveFromAsync(recv);
        GameObject tmp_player = Instantiate(player, new Vector3(110.13f, 36.98f, 54.46f), transform.rotation);
        tmp_player.name = userInfo.playerid;
        tmp_player.GetComponent<Character>().enabled = true;
        
    }
    private void Update()
    {
        read_packet();
    }
    private void read_packet()
    {
        if (que.Count > 0)
        {
            Packet p = que.Dequeue();
            switch (p.packetType)
            {
                case (int)PacketType.OP_GameStatusReply:updateGameStatus(p.body); break;
                case (int)PacketType.OP_GameStart: getEnemyList(); break;
                case (int)PacketType.OP_EnemyListReply:recvEnemyList(p.body);break;
                case (int)PacketType.OP_PlayerMove: playerMovement(p.body);  break;
                case (int)PacketType.OP_CastleDamage:setCsstleHp(p.body); break;
                case (int)PacketType.OP_EnemyHit:EnemyDamage(p.body); break;
                case (int)PacketType.OP_Enemydie:EnemyDamage(p.body);break;
                case (int)PacketType.OP_GameOver:GameOver();break;
                case (int)PacketType.OP_WaveEnd:waveEnd(p.body); break;
                case (int)PacketType.OP_OrbitalStrikeReply:launch(p.body);break;
            }
        }
    }
    private void recv_callback(object o, SocketAsyncEventArgs e)
    {
        string data = Encoding.UTF8.GetString(e.Buffer);
        Array.Clear(buffer, 0, buffer.Length);
        s.ReceiveFromAsync(recv);
        Packet p = JsonUtility.FromJson<Packet>(data);
        que.Enqueue(p);
    }
    private void waveEnd(string data)
    {
        Packet p = new Packet((int)PacketType.OP_GameStatusRequest, false, userInfo.code, "");
        sendData(p);
        NatureStatus n = JsonUtility.FromJson<NatureStatus>(data);
        if (n.code == 1)
        {
            nature.GetComponent<Earthquake>().shakeAmount = n.amount;
            nature.GetComponent<Earthquake>().shakeDuration = n.duration;
        }
        EnemySpawner.GetComponent<EnemySpawner>().wave = n.wave;
        this.getEnemyList();
    }
    private void GameOver()
    {
        nature.GetComponent<Earthquake>().enabled = false;
        castle.GetComponent<CastleHp>().GameOver();
    }
    private void updateGameStatus(string data)
    {
        GameStatus g = JsonUtility.FromJson<GameStatus>(data);
        userInfo.currentWave = g.currentWave;
        castle.GetComponent<CastleHp>().curHp = g.currentHp;
        castle.GetComponent<CastleHp>().maxHp = g.maxHp;
        castle.GetComponent<CastleHp>().SetHpText();
        GameObject.Find("TeamInfo").GetComponent<PlayerInfo>().MoneyTextUpdate(g.money);
    }
    public void castleHpUpgrade()
    {
        Packet p = new Packet((int)PacketType.OP_UpgradeHp,false,userInfo.code,"");
        sendData(p);
    }
    public void repairHp()
    {
        Packet p = new Packet((int)PacketType.OP_RepairHp, false, userInfo.code, "");
        sendData(p);
    }
    public void upgradeDamage()
    {
        Packet p = new Packet((int)PacketType.OP_DamageUp, false, userInfo.code, "");
        sendData(p);
    }
    private void getEnemyList()
    {
        start_btn.SetActive(false);
        Packet p = new Packet((int)PacketType.OP_EnemyListRequest, false, userInfo.code, "");
        sendData(p);
    }
    public void GameStartRequest()
    {
        Packet p = new Packet((int)PacketType.OP_gameStartRequest, false, userInfo.code, "");
        sendData(p);
    }
    private void recvEnemyList(string data)
    {
        EnemySpawner.GetComponent<EnemySpawner>().spawnEnemy(data);
    }
    private void playerMovement(string data)
    {
        PlayerMove m = JsonUtility.FromJson<PlayerMove>(data);
        GameObject other_player = GameObject.Find(m.username);
        if (other_player != null)
        {
            other_player.transform.position = new Vector3(m.x, m.y, m.z);
            other_player.transform.rotation = new Quaternion(m.rx, m.ry, m.rz, other_player.transform.rotation.w);
        }
        else
        {
            other_player = Instantiate(player);
            other_player.name = m.username;
            other_player.transform.position = new Vector3(m.x, m.y, m.z);
            other_player.transform.rotation = new Quaternion(m.rx, m.ry, m.rz, other_player.transform.rotation.w);
        }
    }
    private void EnemyDamage(string body)
    {
        EnemyShoot e = JsonUtility.FromJson<EnemyShoot>(body);
        GameObject.Find(e.id.ToString()).GetComponent<EnemyHP>().GetDamage(e.hp);
    }
    private void setCsstleHp(string data)
    {
        int hp = int.Parse(data);
        castle.GetComponent<CastleHp>().curHp = hp;
        castle.GetComponent<CastleHp>().SetHpText();
    }
    private void launch(string data)
    {
        missile.GetComponent<keystone>().launch();
        GameObject[] monsters = GameObject.FindGameObjectsWithTag("Monster");
        for (int i = 0; i < monsters.Length; i++)
        {
            Destroy(monsters[i].gameObject);
        }
        GameObject.Find("TeamInfo").GetComponent<PlayerInfo>().MoneyTextUpdate(int.Parse(data));
    }
    private void OnDisable()
    {
        s.Close();
    }
}
