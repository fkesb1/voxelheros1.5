﻿using UnityEngine;
using System.Collections;

public class PopupManager : MonoBehaviour {

    public Canvas shopCanvas;

    private bool isOpen = false;

    public void OpenShopCanvas()
    {
        isOpen = !isOpen;
        shopCanvas.enabled = isOpen;
    }
}
