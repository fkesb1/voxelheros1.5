﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Radar : NetworkBehaviour {
    public GameObject radarMap;

    public override void OnStartLocalPlayer()
    {
        radarMap.SetActive(true);
    }
}
